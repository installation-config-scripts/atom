#! /bin/bash

sudo apt install -y atom pylint

apm install language-rust

# Install linters
apm install linter
apm install linter-rust
apm install linter-jshint
apm install atom-easy-jsdoc
apm install linter-htmlhint
apm install linter-pylint

#Helps with documentating
apm install docblockr

#Other
#Change background to the select value
apm install pigments
